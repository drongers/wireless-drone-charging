# Description:

![picture 1, Left/front/top view](drone_chargstat-v1.png)

* Has a hole for the transmitting coil
* Has a small pad for the solar collector
* Should be big enough to hold the rpi and battery inside with all the cables needed

# future plan Description:

* Will be added 2 rfid readers
* Better fitting model with preciesly calculated parts (this was made with datasheet metrics that can be a little bit different in real life)
* A InDesign drawing will be made for the laser cutting