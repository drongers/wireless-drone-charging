# Transmitter circuits 

* For our transmitter circuit first we tried using a BC549C transistor following the uploaded schematic. It could not handle more than 5 voltages and got burnt therefore we had to change it.
It's datasheet: http://www.farnell.com/datasheets/727135.pdf

## BC549C transistor

![picture 1, BC549C transistor](BC549C.jpg)

* Next time we tried using 2N3055 transistor circuit following the uploaded schematic. We did not manage to make the circuit work so we started to try out another circuit. 
It's datasheet: https://www.onsemi.com/pub/Collateral/2N3055A-D.PDF?fbclid=IwAR2wllVSzcW40RYLX9LHO5w6Lmnp1oSS7CfkgLRxQUzQHgi6j5rvuZ4qND0

## 2N3055 transistor

![picture 2, 2N3055 transistor](2N3055AG.jpg)

## 2N3055 schematic

![picture 3, 2N3055-schematic](2N3055AG_Schematic.png)

* The last time we used TIP31C transistor and managed to make it work up to 9 voltages. For our purposes it can handle enough voltages.
It's datasheet: https://www.alldatasheet.com/datasheet-pdf/pdf/618561/NJSEMI/TIP31C.html?fbclid=IwAR0Z_I0T01wreqdlZmjsLFDwSrCJrs-0zyjojupScKF_wyKWcY_NZmvFTIg

## TIP31C transistor

![picture 4, TIP31C transistor](TIP31C.jpg)

## TIP31C schematic

![picture 5, TIP31C-schematic](circuit.png)