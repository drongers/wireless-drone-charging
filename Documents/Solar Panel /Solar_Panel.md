# Schematic:

![picture 1, Solar Panel schematic](SolarPanelSchematic.PNG)

* The resistor we are using is 330 Ohm.

# Solar Panel Specifications:

* Max Power Voltage = 4.0V ±8%
* Max Power Current = 100mA ±8%
* Max Power = 0.4W ±8%


# Tests:

* In e-lab under the lamp the voltage through the LED is 1,82V.
* Outside is 2.05V with resistor and 2,96V without resistor

![picture 2, No Resistor Voltage](NoResistor.png).

* Without the LED and the resistor:
* Using the lamps in the e-lab we got 3.9V output.
* Outside on a clear sunny day the output is 4.84V.

![picture 3, Outside output](OutsideOutput.png)

# Time needed to charge power bank:

![picture 4, Time needed to charge the powerbank](ChargingTime.PNG)

* The powerbank that we are using for tests is 3000mAh and with max power current 100mA from the solar panel it is calculated that we need 30 hours to charge it to full. With no

![picture 5, Formula used to calculate](ChargingFormula.PNG)

* Formula used from the Website.

* Website used to calculate: http://www.meracalculator.com/physics/classical/batteries-charge-time.php

# Datasheet
* https://docs-emea.rs-online.com/webdocs/1407/0900766b81407439.pdf
