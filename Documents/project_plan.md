### Table of Contents ###
----

1. Backgrounds
2. Purpose
3. Goals
4. Schedule
5. Organization
6. Budget and resources
7. Risk assessment 
8. Stakeholders
9. Communication
10. Perspectives
11. Evaluation
12. References



### 1. Backgrounds ###  
----
This is the semester project for ITT3 where we will combine different technologies to develop a wireless charging solution for drones. 

### 2. Purpose ###   
----
The purpose of this project is to create the prototype solution for the end user. The system will charge a drone that lands on it wirelessly. The requirements will be specified in Evaluation section.

### 3. Goals ###  
----
The overall system that is going to be build looks as follows  
![project_overview](project_overview.png)
Our renewable power source will be the sun, we are going to use a sun collector grid. The collected energy will be sent through a Voltage divider and a Filter. From there we store the power in a battery(26-28V). From the battery it goes through a voltage divider again(making it 19-20V). We will use 4 IR sensors to check that the drone has landed correctly on the charging pad, if all the sensors are activated then the magnet will activate and pull the drone to the pad. The electronic switch will activate TX. From the TX the power flows to RX and the on board battery will be charged*** until the Port balancer detects the battery is full and sends a signal to the Sensor´s RFID. The Sensor´s RFID will turn off the electronic switch which disables the TX of wireless charging.

***The charging is happening with the load detached as of this planning phase.
All the maters stated above is matter of change due to final state of project.

Project deliveries are:
* The final product (as described in the Purpose and Evaluation points)  
* Report
* A GitLab project page

### 4. Schedule ###
----  
#### Description
The schedule provides time related information in the semester project. The project will end in week 51, before end of the semester. The project can be extended to the next semester as a final project.

The project lasts **11 weeks**(week 35 - week 50). The working days are **Mondays**.
The time frame within a regular working day is 7 hours with lunch break. The extra hours of work might be required when the team runs into conditions described in the Risk assessment section of this document.

###### Monday schedule
* 8:15 - 8:30 Group meeting
* 8:30 - 9:00 Claim issues & distribute tasks  
* 9:00 - 11:30 Work  
* 11:30 - 12:15 Lunch break  
* 12:15 - 15:00 Work
* 15:00 - 15:30 Reflection on the day

Further information will be added as we progress.
### 5. Organization ###
----  
##### Description
The organization of this project had three elements.

##### The group members:
  
* Levente Atilla Taner  
* Andrej Královič  
* Bence Gyozo Antal  
* Iliyan Stoyadinov  
* Ivan Draganov  
* Soma Gergo Gallai 

##### The project board:

* Ruslan Trifonov  
* Ilias Esmati

##### Customer

* Copenhagen Robots

### 6. Budget and resources ###
----  
No monetary resources are expected. In terms of manpower, only the people in the project group are expected to contribute. Components will be provided by UCL. If special components are required a proposal will be made to Copenhagen Robots to share the expenses.

### 7. Risk assessment ###
----  
The Risk assessment looks at possible risks that could lead to the failure of the project and takes possible solutions into consideration.

### Risks:

- New and unknown communication platforms between first semester students and third semester students.
- New project with goals
- Lack of motivation  
- Too many tasks at once  
- Time management    
- Uncertainties

### Possible solutions:  

   
**Knowledge**

- Seek knowledge / help from classmates.
- Be honest with the goals, never hide your progress! 
- Take the time to explore unknown and new platforms and goals.


**Planning**

- Make sure to keep group meetings, a regular thing. It is important for everyone to be on the same page.
- Never leave issues unresolved!
- Make priorities when planning.
- Make schedules, clear tasks - SMART tasks

**Motivation**

- Take short breaks, walk around and get fresh air.
- Don't overwork yourself in one sitting. 


### 8. Stakeholders ###
----  
The Stakeholders section includes information about stakeholders who are affected by and actively or passively influence the process and outcome of the project.
  
**Customer:**  
- External stakeholders  
- Positive stakeholders  

**Group members:**  
- Internal stakeholders  
- Positive stakeholders  
  
**The project board:**   
- Internal stakeholders  
- Positive stakeholders  

The individual and parties that are the part of the organization is known as Internal Stakeholders. The parties or groups that are not a part of the organization, but gets affected by its activities is known as External Stakeholders.

Negative stakeholders will be negatively affected by the project's success, while positive stakeholder will be favorably (positively) affected by the project's success.


From project stand-point, the Company has priority over other stakeholder groups but from a technical point the project board has priority over the other stakeholder groups.


### 9. Communication ###
----  
Communication is one of the key elements for a project to hold up. Without proper communication between the stakeholders, the project will not end with the right outcome, or maybe with the right outcome but not on time.

**Communication strategy**
*  Team meeting every Monday
*  Constant information about independant stakeholder work
*  Gitlab issues has to be approved by one of the third semesters before they're closed.

**Communication platforms**
*  Gitlab (Primary)
*  Riot and E-mail (Secondary)
*  Facebook (Tertiary - Only used to alert messages on the other platforms)

**Contact info**
<table>
    <tr>
        <th>GitLab username</th>
        <th>Name</th>
        <th>E-mail</th>  
        <th>Semester</th>  
    </tr>
    <tr>
        <td>jambove</td>
        <td>Levente Atilla Taner</td>
        <td><a href="mailto:leve0147@edu.ucl.dk">leve0147@edu.ucl.dk</a></td>  
        <td>Third</td>
    </tr>
    <tr>
        <td>ak98</td>
        <td>Andrej Královič</td>
        <td><a href="mailto:andr14b7@edu.ucl.dk">andr14b7@edu.ucl.dk</a></td>  
        <td>Third</td>
    </tr>
    <tr>
        <td>boriborko</td>
        <td>Bence Gyozo Antal</td>
        <td><a href="mailto:benc0098@edu.ucl.dk">benc0098@edu.ucl.dk</a></td> 
        <td>Third</td>
    </tr>
    <tr>
        <td>iliyanstoyadinov</td>
        <td>Iliyan Stoyadinov</td>
        <td><a href="mailto:iliy0031@edu.ucl.dk">iliy0031@edu.ucl.dk</a></td>  
        <td>Third</td>
    </tr>
    <tr>
        <td>ivan3026</td>
        <td>Ivan Draganov</td>
        <td><a href="mailto:ivan3026@edu.ucl.dk">ivan3026@edu.ucl.dk</a></td>  
        <td>Third</td>
    </tr>
    <tr>
        <td>gallaisoma</td>
        <td>Soma Gergo Gallai</td>
        <td><a href="mailto:gerg0127@edu.ucl.dk">gerg0127@edu.ucl.dk</a></td>  
        <td>Third</td>
</table>

### 10. Perspectives
----  
#### Description
The Perspectives section contains information on who and how will benefit in relation to the project.

####  Group members  
  
The group members will learn how to operate within the group. The communication skills are very important in real life working environment. The GitLab platform will be used as a communication channel and project managment tool. The team will acquire knowledge working in team and using GitLab to manage it. The team will implement the knowledge from the other classes(IoT, networking, communication protocols) and will get familiar with hardware. The group members will also have opportunity to have their internships with the company Copenhagen Robots.

#### Lecturers  
  
The lecturers will be able to benefit from the project also. The knowledge acquired by students and vice versa can be shared. The lecturers will receive feedback, which will enable them to improve in the future. They can improve their business relationship with the company Copenhangen Robots.

#### Company  
  
They get experience working with future graduates and/or interns who will already have insight on their company.

#### Institution  
  
The UCL University College Denmark will be recognised as a higher educational institution which is offering quality education and helps its' students as much as possible to get decent education and fulfilling job in the future.

### 11. Evaluation ###
----  
The evaluation part of the project is an insurance for a quality end product delivered on time for the company.

**First state evaluation**
*  Test the product on our own.
*  Evaluate teamwork
*  Evaluate process
*  Fix errors and problems

**Second state evaluation**
*  Test the product with lectureres to find errors before presenting the product.
*  Evaluate teamwork
*  Evaluate process
*  Fix errors and problems

### 12. References ###
wireless-drone-charging.pdf provided by lecturers

