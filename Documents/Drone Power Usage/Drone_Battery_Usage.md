# Manual Description:

![picture 1, Manual Description](manual_description.jpg)

* It can hold 1000mAh
* The run time will be 12 min approx
* Can be charged with 1A max
* It requires 11.1V
* Charging time is approx 1,5 Hour
* The drones power Usage is 11.1W

# Battery Description:

![picture 2, Outside output](battery_description.jpg)

* Normal Lithium Polymer Battery
* It is a small battery so the weight of it won´t make the drone too heavy

# Conclusion:

* The drones power usage is 11.1W
* We will need to send 11.1V
* Charging time will take approx 1,5 Hour
