#!/usr/bin/env python3

IPPAD = '192.168.11.1'
PORT = 65432        # Port to listen on (non-privileged ports are > 1023)

def read(filename):
    with open(filename) as file:
        data = file.read()
    if "\n" in data:
        return data[:-1]
    else:
        return data

def write(filename,data):
    with open(filename, 'w') as file:
        file.write(data)
    return data

def asw(key):
    asw = {"power-available":"PWR", "no-power": "NPW", "confirmed": "YES", "not-opened": "NOT"}
    return asw[key]

def listen(host,PORT):
    import socket
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((host, PORT))
        #print('Awaiting connection on IP: ',s.getsockname()[0],\
                          ' Port: ',s.getsockname()[1])
        s.listen()
        connection, fromAddress = s.accept()
        #print('Connection from:', fromAddress)
        receivedData = connection.recv(4)
        receivedData = receivedData.decode('utf-8')
    return [receivedData, fromAddress[0]]

def send(host,port,asw):
    import socket
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, PORT))
        s.sendall(asw.encode('utf-8'))
    return(asw)

def main():
    import time
    IPDRONE= listen(IPPAD,PORT)[1]
    print(IPDRONE)
    status = read('status.txt')
    time.sleep(0.5)
    send(IPDRONE,PORT,asw(read('status.txt')))
    #time.sleep(0.3)
    request = listen(IPPAD,PORT)[0]
    time.sleep(0.5)
    if request == 'DONE':
        response = send(IPDRONE,PORT,asw('confirmed'))
        new_status = status
    elif request == "RSRV" and status != "not-opened":
        new_status = write('status.txt','not-opened')
        response = send(IPDRONE,PORT,asw('confirmed'))
    else:
        response = send(IPDRONE,PORT,asw('not-opened'))

    print("Status was: ", status)
    print("Request was: ", request)
    print("Response was: ",response)
    print("New status is: ", new_status)

main()

