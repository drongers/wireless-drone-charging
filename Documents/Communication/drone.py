#!/usr/bin/env python3

IPPAD = '192.168.11.1'      # The pad's IP address
PORT = 65432            # The port to send data to a drone


def msg(key):
    msg = {'status': "STTS", 'reserve': "RSRV", 'done' : "DONE"}
    return msg[key]

def listen(HOST,PORT):
    import socket
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        print('Awaiting connection on IP: ',s.getsockname()[0],\
                          ' Port: ',s.getsockname()[1])
        s.listen()
        connection, fromAddress = s.accept()
        print('Connection from:', fromAddress)
        receivedData = connection.recv(3)
        receivedData = receivedData.decode('utf-8')
    return receivedData

def send(host,port,msg):
    import socket
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, PORT))
        s.sendall(msg.encode('utf-8'))
    return(msg)

def main():
    import time
    import subprocess
    IPDRONE = subprocess.check_output(['bash','-c', "ifconfig wlan0 | grep 192.168 | awk '{print$2}'"]).decode("utf-8")[:-1]
    #IPDRONE = '192.168.11.36'
    send(IPPAD,PORT,msg('status'))
    #time.sleep(0.3)
    status = listen(IPDRONE,PORT)
    time.sleep(0.5)
    if status == "NOT":
        request = send(IPPAD,PORT,msg('done'))
    else:
         request = send(IPPAD,PORT,msg('reserve'))
    #time.sleep(0.5)
    response = listen(IPDRONE,PORT)
    print("Status was: ", status)
    print("Request was: ", request)
    print("Response was: ",response)

main()
