### Purpose ###
----
The code focuses on the drone-pad communication using python sockets between two RPi's. The purpose of the communication scripts(pad.py, drone.py) is to verify the status of the charging pad
and possible reservation of the pad by the drone. The scripts assume that the drone was successfully connected to the pad's access point.

### Pad ###
----
The status is found within the "status.txt" file which must exist on the pad in proper directory.
Status messages:
1. power-available = The charging pad is not occupied and the pad's battery is charged, so the charging process can begin immediately after the drone lands.
2. no-power = The charging pad is not occupied, but the pad's battery is not charged.
3. not-opened = The charging pad is occupied or reserved.

The four known message is "yes", which indicates that the communication finished successfully rather than informing about the pad itself.

These messages are shorten to three bytes each for better manipulation.

### Drone ###
----
The request messages of the drone are:
1. status = requests the pad's status
2. reserve = reserves the pad, which is it communicating with
3. done = closing the communication with no furter requests

These messages are shorten to four bytes of data each for better manipulation.

The full code was tested and it is available in this repository for both(the drone and the pad.)