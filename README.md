# Wireless Drone Charging


<h2>Executive Overview</h2>
The aim of the project is to develop a wireless charging solution for drones for Copenhagen Robots Aps. The student group will be responsible for deliver a working prototype, a working simulation, and provide technical competences.  


<h2>What to do and when?</h2>
We will start the project with creating a project plan and reading up on information, as well as discussing assigned tasks like who when and why.

This markdown file will be edited as in the future first semester students will join.


<h2>Goals</h2>
The goal is to get a better understanding of making a complete product from scratch for a user group and manage the project through GitLab, as well as creating documentation of the product aimed at the stakeholders.


<h2>Communications</h2>
The way that we aim to communicate is in class, as well as Riot, Facebook, and e-mail.  
  
E-mails in below in Team list.

<h2>Documentation</h2>
The documentations include the folder "Documents" that has "project_plan.md" with a more detailed plan on the project.


<h2>Team list</h2>
<table>
    <tr>
        <th>GitLab username</th>
        <th>Name</th>
        <th>E-mail</th>  
        <th>Semester</th>  
    </tr>
    <tr>
        <td>jambove</td>
        <td>Levente Atilla Taner</td>
        <td><a href="mailto:leve0147@edu.ucl.dk">leve0147@edu.ucl.dk</a></td>  
        <td>Third</td>
    </tr>
    <tr>
        <td>ak98</td>
        <td>Andrej Královič</td>
        <td><a href="mailto:andr14b7@edu.ucl.dk">andr14b7@edu.ucl.dk</a></td>  
        <td>Third</td>
    </tr>
    <tr>
        <td>boriborko</td>
        <td>Bence Gyozo Antal</td>
        <td><a href="mailto:benc0098@edu.ucl.dk">benc0098@edu.ucl.dk</a></td> 
        <td>Third</td>
    </tr>
    <tr>
        <td>iliyanstoyadinov</td>
        <td>Iliyan Stoyadinov</td>
        <td><a href="mailto:iliy0031@edu.ucl.dk">iliy0031@edu.ucl.dk</a></td>  
        <td>Third</td>
    </tr>
    <tr>
        <td>ivan3026</td>
        <td>Ivan Draganov</td>
        <td><a href="mailto:ivan3026@edu.ucl.dk">ivan3026@edu.ucl.dk</a></td>  
        <td>Third</td>
    </tr>
    <tr>
        <td>gallaisoma</td>
        <td>Gallai Soma</td>
        <td><a href="mailto:gerg0127@edu.ucl.dk">gerg0127@edu.ucl.dk</a></td>  
        <td>Third</td>
</table>
        
